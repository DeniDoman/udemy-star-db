import { createContext } from "react";

const SwapiContext = createContext();

export default SwapiContext;
