import React from "react";
import useData from "../../hooks/use-data/use-data";
import Spinner from "../../components/spinner/spinner";
import ErrorIndicator from "../../components/error-indicator/error-indicator";

const withLoadData = (Component, getData, args) => props => {
  const [data, isLoading, isError] = useData(getData, args);

  if (isLoading) return <Spinner />;
  if (isError) return <ErrorIndicator />;
  // eslint-disable-next-line react/jsx-props-no-spreading
  return <Component data={data} {...props} />;
};

export default withLoadData;
