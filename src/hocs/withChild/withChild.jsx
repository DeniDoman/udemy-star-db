import React from "react";

const withChild = (Component, child) => props => {
  // eslint-disable-next-line react/jsx-props-no-spreading
  return <Component {...props}>{child}</Component>;
};

export default withChild;
