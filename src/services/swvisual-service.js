export default class SwVisualService {
  apiBase = "https://starwars-visualguide.com/assets/img";

  getPlanetImgLink = id => {
    return `${this.apiBase}/planets/${id}.jpg`;
  };

  getPersonImgLink = id => {
    return `${this.apiBase}/characters/${id}.jpg`;
  };

  getStarshipImgLink = id => {
    return `${this.apiBase}/starships/${id}.jpg`;
  };
}
