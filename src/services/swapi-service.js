export default class SwapiService {
  apiBase = "https://swapi.co/api";

  getResource = async url => {
    const res = await fetch(`${this.apiBase}${url}`);
    if (!res.ok)
      throw new Error(`Couldn't fetch ${url}, received ${res.status}`);
    return res.json();
  };

  getAllPeople = async () => {
    const res = await this.getResource("/people/");
    return res.results.map(this.transformPerson);
  };

  getPerson = async id => {
    const res = await this.getResource(`/people/${id}`);
    return this.transformPerson(res);
  };

  getAllPlanets = async () => {
    const res = await this.getResource("/planets/");
    return res.results.map(this.transformPlanet);
  };

  getPlanet = async id => {
    const res = await this.getResource(`/planets/${id}`);
    return this.transformPlanet(res);
  };

  getAllStarships = async () => {
    const res = await this.getResource("/starships/");
    return res.results.map(this.transformStarship);
  };

  getStarship = async id => {
    const res = await this.getResource(`/starships/${id}`);
    return this.transformStarship(res);
  };

  extractId = item => {
    const regexp = /\/([0-9]*)\/$/;
    return item.url.match(regexp)[1];
  };

  transformPlanet = planet => {
    return {
      name: planet.name,
      population: planet.population,
      rotationPeriod: planet.rotation_period,
      diameter: planet.diameter,
      id: this.extractId(planet)
    };
  };

  transformStarship = starship => {
    return {
      id: this.extractId(starship),
      name: starship.name,
      model: starship.model,
      manufacturer: starship.manufacturer,
      costInCredits: starship.costInCredits,
      length: starship.length,
      crew: starship.crew,
      passengers: starship.passengers,
      cargoCapacity: starship.cargoCapacity
    };
  };

  transformPerson = person => {
    return {
      id: this.extractId(person),
      name: person.name,
      gender: person.gender,
      birthYear: person.birth_year,
      eyeColor: person.eye_color
    };
  };
}
