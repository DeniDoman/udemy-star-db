import "./app.css";
import React, { useState } from "react";
import Header from "../header/header";
import RandomPlanet from "../random-planet/random-planet";
import SwapiContext from "../../contexts/swapi-service-context/swapi-service-context";
import SwapiService from "../../services/swapi-service";

import {
  PersonList,
  PlanetList,
  StarshipList
} from "../sw-components/item-lists";
import {
  PersonDetails,
  PlanetDetails,
  StarshipDetails
} from "../sw-components";

const App = () => {
  const [isRandomPlanetEnabled, setIsRandomPlanetEnabled] = useState(true);
  const [selectedPersonId, setSelectedPersonId] = useState();
  const [selectedPlanetId, setSelectedPlanetId] = useState();
  const [selectedStarshipId, setSelectedStarshipId] = useState();

  const swapiService = new SwapiService();

  const onToggleRandomPlanet = () => {
    setIsRandomPlanetEnabled(!isRandomPlanetEnabled);
  };

  const onPersonSelect = id => {
    setSelectedPersonId(id);
  };

  const onPlanetSelect = id => {
    setSelectedPlanetId(id);
  };

  const onStarshipSelect = id => {
    setSelectedStarshipId(id);
  };

  return (
    <SwapiContext.Provider value={swapiService}>
      <div>
        <Header />
        {isRandomPlanetEnabled && <RandomPlanet />}

        <button
          type="button"
          className="btn btn-warning"
          onClick={() => onToggleRandomPlanet()}
        >{`${isRandomPlanetEnabled ? "Hide" : "Show"} random planet`}</button>

        <PersonDetails itemId={2} />
        <PlanetDetails itemId={2} />
        <StarshipDetails itemId={5} />

        <PersonList onItemSelect={onPersonSelect} />

        <PlanetList onItemSelect={onPlanetSelect} />

        <StarshipList onItemSelect={onStarshipSelect} />
      </div>
    </SwapiContext.Provider>
  );
};

export default App;
