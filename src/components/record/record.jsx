import React from "react";
import PropTypes from "prop-types";

const Record = ({ data, field, label }) => {
  return (
    <>
      <li className="list-group-item">
        <span className="term">{label}</span>
        <span>{data[field]}</span>
      </li>
    </>
  );
};

export default Record;

Record.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string.isRequired
  }).isRequired,
  field: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired
};
