import React from "react";
import PropTypes from "prop-types";
import "./item-details.css";

const ItemDetails = ({ children, data, image }) => {
  return (
    <div className="item-details card">
      <img className="item-image" src={image} alt="Item" />
      <div className="card-body">
        <h4>{data.name}</h4>
        <ul className="list-group list-group-flush">
          {React.Children.map(children, child => {
            return React.cloneElement(child, { data });
          })}
        </ul>
      </div>
    </div>
  );
};

ItemDetails.propTypes = {
  children: PropTypes.arrayOf(PropTypes.node).isRequired,
  data: PropTypes.shape({
    name: PropTypes.string.isRequired
  }).isRequired,
  image: PropTypes.string.isRequired
};

export default ItemDetails;
