import React, { useReducer, useEffect } from "react";
import Spinner from "../spinner/spinner";
import ErrorIndicator from "../error-indicator/error-indicator";
import PlanetView from "./planet-view/planet-view";
import SwapiService from "../../services/swapi-service";
import "./random-planet.css";

const swapiService = new SwapiService();

const planetReducer = (state, action) => {
  switch (action.type) {
    case "loaded":
      return { ...state, ...action.payload, loading: false, error: false };
    case "error":
      return { ...state, loading: false, error: true };
    default:
      return { ...state };
  }
};

const RandomPlanet = () => {
  const [state, dispatch] = useReducer(planetReducer, {
    loading: true,
    error: false
  });

  const onPlanetLoaded = planet => {
    dispatch({ type: "loaded", payload: planet });
  };

  const onError = () => {
    dispatch({ type: "error" });
  };

  const updatePlanet = () => {
    const pid = Math.floor(Math.random() * 17) + 2;
    swapiService
      .getPlanet(pid)
      .then(onPlanetLoaded)
      .catch(onError);
  };

  useEffect(() => {
    updatePlanet();
    const interval = setInterval(updatePlanet, 5000);
    return () => {
      clearInterval(interval);
    };
  }, []);

  if (state.error) return <ErrorIndicator />;

  if (state.loading) return <Spinner />;

  return (
    <div className="random-planet jumbotron rounded">
      <PlanetView state={state} />
    </div>
  );
};

export default RandomPlanet;
