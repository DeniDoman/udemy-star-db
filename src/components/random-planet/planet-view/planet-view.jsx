import React from "react";
import { PropTypes } from "prop-types";
import SwVisualService from "../../../services/swvisual-service";
import "../random-planet.css";
import "./planet-view.css";

const swVisualService = new SwVisualService();

const PlanetView = ({ state }) => {
  return (
    <>
      <img
        className="planet-image"
        src={swVisualService.getPlanetImgLink(state.id)}
        alt="planet"
      />
      <div>
        <h4>{state.name}</h4>
        <ul className="list-group list-group-flush">
          <li className="list-group-item">
            <span className="term">Population:</span>
            <span>{state.population}</span>
          </li>
          <li className="list-group-item">
            <span className="term">Rotation Period:</span>
            <span>{state.rotationPeriod}</span>
          </li>
          <li className="list-group-item">
            <span className="term">Diameter:</span>
            <span>{state.diameter}</span>
          </li>
        </ul>
      </div>
    </>
  );
};

PlanetView.propTypes = {
  state: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    population: PropTypes.string.isRequired,
    rotationPeriod: PropTypes.string.isRequired,
    diameter: PropTypes.string.isRequired
  }).isRequired
};

export default PlanetView;
