import PersonDetails from "./person-details/person-details";
import PlanetDetails from "./planet-details/planet-details";
import StarshipDetails from "./starship-details/starship-details";

export { PersonDetails, PlanetDetails, StarshipDetails };
