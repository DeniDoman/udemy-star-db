import React, { useContext } from "react";
import { PropTypes } from "prop-types";
import ItemDetails from "../../item-details/item-details";
import Record from "../../record/record";
import withLoadData from "../../../hocs/withLoadData/withLoadData";
import SwapiContext from "../../../contexts/swapi-service-context/swapi-service-context";
import SwVisualService from "../../../services/swvisual-service";

const swvisualService = new SwVisualService();

const { getPlanetImgLink } = swvisualService;

const Template = ({ data }) => {
  return (
    <ItemDetails data={data} image={getPlanetImgLink(data.id)}>
      <Record label="Population:" field="population" />
      <Record label="Diameter:" field="diameter" />
    </ItemDetails>
  );
};

const PlanetDetails = ({ itemId }) => {
  const { getPlanet } = useContext(SwapiContext);
  const Component = React.memo(withLoadData(Template, getPlanet, itemId));
  return <Component />;
};

export default PlanetDetails;

Template.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.string.isRequired
  }).isRequired
};

PlanetDetails.propTypes = {
  itemId: PropTypes.number.isRequired
};
