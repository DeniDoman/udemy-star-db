import React, { useContext } from "react";
import { PropTypes } from "prop-types";
import ItemDetails from "../../item-details/item-details";
import Record from "../../record/record";
import withLoadData from "../../../hocs/withLoadData/withLoadData";
import SwapiContext from "../../../contexts/swapi-service-context/swapi-service-context";
import SwVisualService from "../../../services/swvisual-service";

const swvisualService = new SwVisualService();

const { getPersonImgLink } = swvisualService;

const Template = ({ data }) => {
  return (
    <ItemDetails data={data} image={getPersonImgLink(data.id)}>
      <Record label="Gender:" field="gender" />
      <Record label="Eye color:" field="eyeColor" />
    </ItemDetails>
  );
};

const PersonDetails = ({ itemId }) => {
  const { getPerson } = useContext(SwapiContext);
  const Component = withLoadData(Template, getPerson, itemId);
  return <Component />;
};

export default PersonDetails;

Template.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.string.isRequired
  }).isRequired
};

PersonDetails.propTypes = {
  itemId: PropTypes.number.isRequired
};
