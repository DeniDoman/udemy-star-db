import React from "react";
import { PropTypes } from "prop-types";
import ItemList from "../item-list/item-list";
import withLoadData from "../../hocs/withLoadData/withLoadData";
import SwapiService from "../../services/swapi-service";
import withChild from "../../hocs/withChild/withChild";

const swapiService = new SwapiService();
const { getAllPeople, getAllStarships, getAllPlanets } = swapiService;

const renderName = ({ name }) => <span>{name}</span>;
const renderModelAndName = ({ model, name }) => (
  <span>
    {name} ({model})
  </span>
);

const PersonList = withLoadData(withChild(ItemList, renderName), getAllPeople);
const PlanetList = withLoadData(withChild(ItemList, renderName), getAllPlanets);
const StarshipList = withLoadData(
  withChild(ItemList, renderModelAndName),
  getAllStarships
);

export { PersonList, PlanetList, StarshipList };

renderName.propTypes = {
  name: PropTypes.string.isRequired
};

renderModelAndName.propTypes = {
  name: PropTypes.string.isRequired,
  model: PropTypes.string.isRequired
};
