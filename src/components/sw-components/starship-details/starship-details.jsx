import React, { useContext } from "react";
import { PropTypes } from "prop-types";
import ItemDetails from "../../item-details/item-details";
import Record from "../../record/record";
import withLoadData from "../../../hocs/withLoadData/withLoadData";
import SwapiContext from "../../../contexts/swapi-service-context/swapi-service-context";
import SwVisualService from "../../../services/swvisual-service";

const swvisualService = new SwVisualService();

const { getStarshipImgLink } = swvisualService;

const Template = ({ data }) => {
  return (
    <ItemDetails data={data} image={getStarshipImgLink(data.id)}>
      <Record label="Cost:" field="costInCredits" />
      <Record label="Length:" field="length" />
    </ItemDetails>
  );
};

const StarshipDetails = ({ itemId }) => {
  const { getStarship } = useContext(SwapiContext);
  const Component = React.memo(withLoadData(Template, getStarship, itemId));
  return <Component />;
};

export default StarshipDetails;

Template.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.string.isRequired
  }).isRequired
};

StarshipDetails.propTypes = {
  itemId: PropTypes.number.isRequired
};
