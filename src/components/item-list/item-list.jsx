import React from "react";
import PropTypes from "prop-types";
import "./item-list.css";

const ItemList = ({ children, data, onItemSelect }) => {
  const renderList = data.map(item => {
    return (
      <li key={item.id} className="list-group-item">
        <button
          type="button"
          onClick={() => {
            onItemSelect(item.id);
          }}
        >
          {children(item)}
        </button>
      </li>
    );
  });

  return <ul className="item-list list-group">{renderList}</ul>;
};

ItemList.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  ).isRequired,
  onItemSelect: PropTypes.func.isRequired,
  children: PropTypes.func.isRequired
};

export default ItemList;
