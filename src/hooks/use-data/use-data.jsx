import { useState, useEffect } from "react";

const useData = (source, args) => {
  const [data, setData] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    const fetch = async () => {
      setIsLoading(true);
      setIsError(false);
      try {
        setData(await source(args));
      } catch (e) {
        setIsError(true);
      }
      setIsLoading(false);
    };
    fetch();
  }, [source, args]);

  return [data, isLoading, isError];
};

export default useData;
